module.exports = {
  env: {
    browser: true,
    es2021: true
  },

  //extends: 'standard',
  extends :'eslint:recommended',
  overrides: [
  ],
  parserOptions: {
    ecmaVersion: 'latest'
  },
  rules: {

     "no-console": "error",
     "camelcase": "error",

  },

}
