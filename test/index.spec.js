var { suma, pow } = require("../index");
var chaiHttp = require('chai-http');
var chai = require("chai");
chai.use(chaiHttp);
var convert = require('xml-js');
/* describe("PRUEBAS UNITATARIAS COMPARACION DATOS HTTP: ", () => {
  it("COMPARAR DATOS DE UNA CONSULTA TAREA XXX: ", (done) => {
    chai.request('190.60.28.254:8383')
      .get('/clinica/paginas/accionesXml/buscarGrillaVentanaPaciente_xml.jsp?idQuery=27&parametros=_-x.X.x_-x.X.x_-1085309234_-1085309234_-x.X.x&_search=false&nd=1679191979107&rows=-1&page=1&sidx=&sord=asc')
      .set('Content-Type', 'application/x-www-form-urlencoded')

      .end((err, res) => {

        var respuesta = [
          { _cdata: '1' },
          { _cdata: '34933' },
          { _cdata: '95200187976' },
          { _cdata: 'CC-1085309234-ANGULO MIRAMA CHRISTIAN CAMILO' }
        ]
        var result1 = convert.xml2js(res.text, { compact: true, spaces: 4 });
        var result2 = result1.rows.row.cell





        chai.expect(result2).to.deep.equal(respuesta);
        done()

      })
  });



});
 */
describe("PRUEBAS UNITARIAS FUNCIONES: ", () => {
  it("PRUEBA FUNCION SUMA", (done) => {
    result = suma(1, 1)
    chai.assert.equal(result, 2);

    done()
  })

  describe("FUNCION ELEVADO A LA 3:", () => {
    function makeTest(x) {
      let expected = x * x * x;
      it(`NUMERO ${x} ELEVADO A 3 ES: ${expected}`, function () {
        chai.assert.equal(pow(x, 3), expected);
      });
    }
    for (let x = 1; x <= 5; x++) {
      makeTest(x);
    }
  })



})
